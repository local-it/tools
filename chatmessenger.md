# Chat Messenger

Bekannte Messenger wie Whatsapp oder Telegram haben
nicht nur datenschutzrechtliche Probleme, sondern sind
hauptsächlich für die Kommunikation zwischen Einzelpersonen oder in kleinen unmoderierten Gruppen ausgelegt und ihnen fehlen Funktionen, die für die strukturierte
Zusammenarbeit in Gruppen und Teams wichtig sind. Für
den einfachen Umstieg auf eine Open-Source-Alternative
zu Whatsapp ist Signal mit ähnlichem Funktionsumfang
schon ein guter Anfang. Die Kommunikation ist grundsätzlich verschlüsselt und das Tool ist Open-Source, läuft
aber über zentrale Server, die von einer Stiftung in den
USA betrieben werden.
Ein schon recht altes, aber vor allem unter technikaffinen
Menschen weit verbreitetes Chat-Protokoll ist das Extensible Messaging and Presence Protocol (XMPP). Es ist
ähnlich wie E-Mail dezentral aufgebaut. Jede:r kann einen
eigenen Server betreiben und mit anderen Nachrichten
austauschen, das wird auch als „Föderieren“ bezeichnet.
Auch gibt es mehrere unterschiedliche Client- und Server-Implementierungen, die untereinander kompatibel sind.
Einen einfach zu installierenden Server und App bietet
z.B. Snikket.
Andere alternative Messenger für spezielle Anforderungen sind Briar oder tox.chat. Sie setzen auf sog. P2P-Kommunikation und benötigen damit keinen Server. Sie
sind besonders geeignet, wenn absolute Vertraulichkeit
z.B. unter einem autoritären Regime notwendig ist.
Für die Kommunikation in Teams werden aber häufig
mehr Funktionen benötigt und eine Trennung von privater Kommunikation und Team-Kommunikation ist oft erwünscht. Daher vergleichen wir im Folgenden verschiedene Messenger Plattformen, die für die Anwendung in
Teams optimiert sind.

## Messenger Plattformen

### Überblick

Wir haben vier unterschiedliche Plattformen ausprobiert und in Vergleich mit sehr beliebten, aber proprietären Messenger Plattform Slack[^slack] gesetzt.

|              | Matrix                | Mattermost      | Rocketchat                     | Zulip            |
| ------------ | --------------------- | --------------- | ------------------------------ | ---------------- |
| Lizenz       | 🕊️                    | 🕊️ / 💲         | 🕊️ / 💲                        | 🕊️               |
|              | Apache 2.0            | MIT/EPL         | MIT/EPL                        | Apache 2.0       |
| Installation | S                     | S               | S                              | M                |
| SSO          | ✔️                    | 💲              | ✔️                             | ✔️               |
| Entwicklung  | Matrix.org Foundation | Mattermost Inc. | Rocket.Chat Technologies Corp. | Kandra Labs Inc. |

### Funktioneller Vergleich

Im Folgenden werden Funktionen zwischen Slack und
Open-Source Alternativen verglichen.

1. Threads: Bieten die Möglichkeit in einem Gruppenchat auf gezielte Themen zu antworten und das Thema zu verfolgen.
2. Ansichten: Es gibt zusätzliche Ansichten für ungelesene Nachrichten, Benachrichtigungen und Erwähnungen.
3. Videotelefonie: 1-zu-1 und Gruppentelefonate.
4. Sprachnachrichten: Werden vor allem im Privaten
   Kontext immer beliebter.
5. Globale Suchfunktion: Die über Chats und Räume
   hinweg das Finden von Nachrichten ermöglicht.
6. Communities: User und (Gruppen-) Chats können
   thematisch oder nach Projekten gruppiert werden.
7. Integration von anderen Apps wie z.B. Umfragen,
   Emails, Projektmanagement.
8. Föderierbar: User von einem Server können mit
   Usern von einem anderen Server kommunizieren.
   Das impliziert, dass der Dienst dezentral aufgebaut
   ist und den Betrieb eigener Server-Instanzen erlaubt.
9. E2E Verschlüsselung: Ende-zu-Ende Verschlüsselung
   bedeutet, dass wirklich nur die User:innen ihre Nachrichten lesen können. Auch ein:e System-Adminstrator:in hat keinen Zugriff auf die Inhalte. Das ist
   besonders wichtig für Gruppen, die sehr hohen Wert
   auf die Vertraulichkeit ihrer Nachrichten legen.

| Funktionen                      | Slack            | Matrix, Element | Mattermost           | Rocketchat        | Zulip                                  |
| ------------------------------- | ---------------- | --------------- | -------------------- | ----------------- | -------------------------------------- |
| 1. Threads                      | ✔️               | in Beta         | ✔️                   | ✔️                | ✔️                                     |
| 2. Ansichten                    | ✔️               | ❌              | ❌                   | ✔️                | ✔️                                     |
| 3. Videotelefonie / Konferenzen | Zoom integr.     | ✔️ / Jitsi      | Jitsi                | ✔️ Jitsi/BBB Beta | ❌ link zu Jitsi/BBB/Zoom              |
| 4. Sprachnachrichten            | 💲               | ✔️              | ❌                   | ✔️                | ❌                                     |
| 5. Globale Suchfunktion         | ✔️               | teilweise       | ✔️                   | ❌                | ✔️                                     |
| 6. Communities                  | ✔️               | ✔️              | ✔️                   | ❌                | ✔️                                     |
| 7. Integrationen                | ✔️               | ✔️              | ✔️                   | ✔️                | ✔️                                     |
| 8. Föderierbar                  | 💲 Slack-Connect | ✔️              | ❌ über Matterbridge | ❌ (Alpha)        | ❌ (Bridges zu Matrix, Slack, weitere) |
| 9. E2E Verschlüsselung          | ❌               | ✔️              | ❌                   | ✔️                | ❌                                     |

### Matrix, Element

Matrix[^matrix] steht in erster Linie für ein auf HTTP basierendes
offenes Protokoll für Messaging. Dieses Protokoll wird
durch verschiedene Server- und Client-Implementierungen umgesetzt. Hier wurden Matrix-Synapse als Server
und Element als Client getestet. Im Vergleich zu anderen
Lösungen ist Grundbestandteil des Protokolls die sog. Föderierung, womit alle Matrix-Server, egal wer sie betreibt,
untereinander kommunizieren können. Das Protokoll ist
vergleichbar mit dem dezentralen Ansatz von E-Mail-Servern. Matrix-Server leiten Nachrichten zu dem jeweils
zuständigen Matrix-Server weiter, bei dem der:die Benutzer:in registriert ist. Die Föderation kann für einen Server
aber auch deaktiviert werden, um die Kommunikation auf
eine Organisation zu beschränken. Aktuell können nur
1-zu-1 Videoanrufe direkt über Matrix/Element durchgeführt werden, für Gruppentelefonate wird momentan eine
Jitsi-Integration verwendet. Hier ist aber auch eine native
Umsetzung bereits in der Testphase.

Durch das offene Matrix-Protokoll gibt es noch eine Vielzahl an weiteren Implementierungen und Erweiterungen
für Räume (Extensions) oder sog. Bots und Bridges, die
weitere Funktionen oder Anbindungen zu anderen Messenger-Plattformen ermöglichen. Die Qualität der Erweiterungen kann aber stark schwanken.
Ein häufiges Problem in der praktischen Anwendung von
Element als Client ist, dass User:innen vergessen ihre
Passphrase für die Wiederherstellung von E2E-Encryption-Keys zu speichern. Dadurch ist schon häufiger User:innen ihre gesamte Historie abhanden gekommen.

### Mattermost

Mattermost[^mattermost] ist primär auf eine themenbezogene Kommunikation innerhalb von Organisationen ausgelegt und
als Slack-Alternative anzusehen. Die Lizenzbeschreibungen enthalten einige undurchsichtige Formulierungen,
welche Funktionen jetzt und in Zukunft in der Communityund welche in der Enterprise-Lizenz vorhanden sein werden. Dies hinterlässt ein paar Fragezeichen bezüglich der
zukünftigen Verfügbarkeit als Open-Source-Tool. SSO und
weitere Features sind nur in der kommerziellen Professional-Lizenz enthalten. Mit der Matterbridge kann Mattermost an andere Chat-Portale und Lösungen angebunden
werden. Es ist aber auf die Kommunikation innerhalb der
eigenen Organisation fokussiert. Mattermost gliedert
sich in Channels und Direct Messages auf. Über einen
angebotenen Marketplace (Apps) kann der Funktionsumfang erweitert werden.
Grundsätzlich macht Mattermost einen soliden Eindruck,
die Bedienung funktioniert reibungslos und alle wesentlichen Features sind enthalten. Die Bedienung der globalen
Suchfunktion ist wenig intuitiv. Sprachnachrichten sind
nur teilweise über ein Plugin nachrüstbar (keine Mobilunterstützung). Dafür wird man mit einem Tutorial gut in den
Messenger eingeführt und findet sich schnell zurecht. Die
Integration des Kanban-Boards Focal ist vielversprechend
und kann Mattermost zu einer Komplettlösung für manche Teams machen.

### Rocket.Chat

Rocket.Chat[^rocketchat] positioniert sich selbst als Slack-Alternative
und nennt sich die größte Open-Source-Kommunikationsplattform der Welt. Rocket.Chat unterscheidet zwischen
Kanälen und Diskussionen, welche Kanälen zugeordnet
sind. So kann ein Hauptthema in mehrere Unterthemen unterteilt werden. Außerdem gibt es die Möglichkeit Teams
zu erstellen, die eine Gruppe von Benutzer:innen bilden.
Es ist aber nicht möglich wie bei anderen Messengern
Bereiche zu erstellen, die nur bestimmte Chats anzeigen
und für mehr Übersichtlichkeit sorgen. Ein Marketplace
(Apps) ermöglicht zudem eine modulare Erweiterung. Interessant kann die Anbindung an andere Messenger wie
Instagram, WhatsApp usw. und auch klassische E-Mail Postfächer sein. An einer nahtlosen Ankopplung an das
Matrix-Netzwerk wird zur Zeit gearbeitet.

### Zulip

Zulip[^zulip] beinhaltet direkt eine Importfunktion zur Migration
von Slack, Mattermost, Gitter oder Rocket.Chat. Der Aufbau der Basisfunktionen der Weboberfläche basiert auf
Chat-Nachrichten direkt zwischen Benutzer:innen und so
genannten Streams. Die Streams können themenbezogen angelegt werden und mehrere Benutzer:innen können
so z.B. als Projektteams oder zu einem Thema innerhalb
dieser Streams per Chat kommunizieren. Innerhalb von
Streams können Topics weitere Unterkategorien bilden.
So kann z.B. ein Projekt ein Stream sein und die einzelnen Themen des Projektes können in Topics diskutiert
werden. Funktionen, wie Erwähnungen, sind zur besseren
Benachrichtigung von angesprochenen Benutzer:innen
ebenfalls vorhanden.
Das Design und die etwas von bekannten Tools abweichende Funktionsweise machen den Einstieg in das Tool
etwas schwer. Der starke Fokus auf Topics und andere
Funktionen, wie Ansichten für Erwähnungen, versprechen
bessere Übersicht. Insgesamt macht Zulip aber eher den
Eindruck für technisch versiertere Gruppen ausgerichtet
zu sein.

[^slack]: https://slack.com
[^matrix]: https://matrix.org/
[^mattermost]: https://mattermost.com/
[^rocketchat]: https://rocket.chat/
[^zulip]: https://zulip.com/
