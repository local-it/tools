# Tools

## Einleitung

Die Toolanalyse hat das Ziel, einen Überblick über bereits
bestehende Open-Source-Tools vorzustellen, zu untersuchen und funktionell zu vergleichen. Damit soll das
im vorangegangenen Berichtsteil ermittelte mangelnde
Wissen über Open-Source-Alternativen und der Bedarf
nach einer Entscheidungshilfe zur Auswahl adressiert
werden. Dabei werden die in der Bedarfsanalyse ermittelten Themenbereiche untersucht. Der Fokus liegt dabei
auf der Kollaborations-Fähigkeit. Das heißt, dass Teams
standortunabhängig miteinander arbeiten können. Daher
werden überwiegend Web-basierte Lösungen betrachtet,
die ohne eine lokale Installation auf dem PC/Tablet auskommen. Die Analyse teilt sich nach den Anwendungs-
feldern Dateiablage, Chat / Messenger, Dokumentenbearbeitung, E-Mail, Kalender, Video-Konferenzsysteme
und Kanban-Boards / Projektmanagement-Tools auf. Das
Anwendungsfeld wird jeweils kurz erläutert und mehrere
Lösungen vorgestellt und verglichen.
Um zu entscheiden welches Tool das Richtige für eine Organisation ist, muss immer der konkrete Anwendungsfall
und die Rahmenbedingungen der Organisation betrachtet
werden. Ein vollständiger Entscheidungsleitfaden würde
den Rahmen dieser Arbeit zwar sprengen, es werden aber
als Anhaltspunkt allgemeine und funktionelle Kriterien
für die Tools mitgegeben. Allgemeine Kriterien sind die
Lizenz, Installationsaufwand, Single-Sign-On (SSO) und
Entwicklung.
Je Anwendungsfeld werden spezifische Funktionen verglichen. Dazu dienen bereits etablierte kommerzielle Lösungen als Referenz, um Schwächen und Stärken der
Open-Source-Alternativen aufzuzeigen. Ergänzt wird die
Analyse um Eindrücke und Erfahrungen von User:innen.

### Allgemeine Kriterien

Um trotz der großen Unterschiede zwischen den Tools ein
gewisses Maß an Vergleichbarkeit herzustellen, werden
die Tools in Bezug auf allgemeine Kriterien Anwendungsfeld-übergreifend untersucht. Weiterhin sollen die Kriterien als Anhaltspunkt für die Auswahl von Tools dienen,
wobei dabei die Rahmenbedingungen einer Organisation
ausschlaggebend sind. Das können die Größe, Anzahl
Haupt-/Ehrenamtlicher Mitglieder, Affinität zu Digitalisierung und Ressourcen für die Einarbeitung, finanzielle
Mittel sowie die Organisationsstruktur sein. Beispielweise hat eine kleine freiwillige Feuerwehr eine andere
Ausgangslage als eine deutschlandweit agierende Klimaschutzbewegung.

### Lizenz

Die Begriffe Open-Source und Freie Software werden im
gesellschaftlichen Diskurs häufig synonym verwendet.
Dabei besteht allerdings ein wichtiger Unterschied, der in
der Toolanalyse genauer beleuchtet werden soll. Denn der
offene Zugang zum Quellcode ist nur eine von vier Freiheiten,
die die Free Sofware Foundation definiert[^gnu]:

1. Das Programm auszuführen, wie man möchte, für
   jeden Zweck.
2. Die Funktionsweise des Programms zu untersuchen
   und eigenen Datenverarbeitungsbedürfnissen anzupassen.
3. Das Programm zu redistribuieren und damit Mitmenschen zu helfen.
4. Das Programm zu verbessern und diese Verbesserungen der
   Öffentlichkeit freizugeben, damit die gesamte Gesellschaft davon profitiert.

Ob es sich bei einem Tool also um Freie Software handelt,
lässt sich erst nach Prüfung der Lizenz erkennen. Einige
Unternehmen verwenden ein „Dual-License“ -Geschäftsmodell.
Dabei wird die Software in zwei Lizenzen unterteilt. Eine „Community Edition“ (CE),
die Freie Software ist und eine „Enterprise License“ (EPL), die kostenpflichtig
ist. Dabei bietet die EPL häufig weitere Features, die aber
keine Freie Software sind. Vor dem Einsatz von Software
mit einer „Dual License“ sollte also beachtet werden, ob
alle nötigen Features von der freien Variante abgedeckt
werden.

Grundsätzlich sind die oben genannten „Vier Freiheiten“
oder die vergleichbaren Anforderungen der „Open Source
Initiative“[^osd] Voraussetzung für eine echte digitale Souveränität.
Für den Betrieb der Software ist die Art der freien Software-Lizenz
weniger wichtig. Sobald Modifikationen an
einer Software vorgenommen werden sollen, ist es allerdings
interessant abzuschätzen, wie restriktiv oder
kombinierbar eine Lizenz ist. Restriktive Lizenzen wie die
GPL-3.0 folgen der „Copyleft“ Methode. Diese verlangt,
dass alle Modifikationen der Software wieder unter freier
Software-Lizenz gestellt werden müssen. Das fördert die
Verbreitung von Freier Software, macht sie aber auch weniger
kompatibel zu anderen Lizenzen für Freie Software.
Als besonders „permissive“ Lizenz sei hier die MIT-Lizenz
erwähnt[^mit]. Für mehr Details zu den Lizenzen sei auf eine
Übersicht von „choosealicense“ [^choosealicense] verwiesen.

### Installation

Der Aufwand für die Installation eines Tools kann ein
entscheidendes Kriterium sein, wenn es darum geht, sie
in ehrenamtlicher Arbeit ohne IT-Abteilung zu betreiben.
Grade bei der Installation zeigt sich, wie viele Komponenten
und Abhängigkeiten ein Tool hat. In vielen Fällen ist
die Installation aber auch schon gut automatisiert und
kann einiges an Aufwand damit abfangen. Häufig deutet
ein aufwändiger Installationsprozess auch auf einen hohen
Aufwand bei der Wartung der Installation hin. Da es schwer ist,
den Installationsaufwand quantitativ anzugeben, wird hier auf ein in der Softwareentwicklung übliches
Verfahren zurückgegriffen: Aufwand in T-Shirt-Größen
schätzen:

- **XL**: Extra Large, das Tool hat noch keine oder nur
  wenig Automatisierung. Es kann per Binary oder Paketmanager installiert werden und Abhängigkeiten
  wie z.B. eine Datenbank müssen manuell eingerichtet oder ganze Konfigurationsdateien manuell angepasst werden. Manchmal müssen sogar noch die
  Quellen übersetzt werden.
- **L**: Large, das Tool kann per automatischem Installer
  betrieben werden und Abhängigkeiten werden automatisch gelöst. Konfigurationen und Anpassungen,
  um das Tool gemeinsam mit anderen Anwendungen
  auf einem Server zu installieren, sind aber noch manuelle Arbeit.
- **M**: Medium, die Installation ist schon teil-automatisiert, es müssen aber noch weitere Anpassungen gemacht werden, bis die Anwendung vollständig funktioniert.
- **S**: Small, das Tool wird überwiegend automatisiert installiert und kann mit einer Selfhosting-Solution (z.B. Co-op Cloud oder Yunohost) betrieben werden. Es
  müssen nur noch Kleinigkeiten angepasst werden.

Bei der serverseitigen Installation der Tools wurde zusätzlich besonderer Wert auf den Betrieb in containerisierter
Umgebung gelegt. Sogenannte „Container“ bieten viele
technische Vorteile. Einer ist, dass die Entwickler:innen
eines Tools vorgeben können, in welcher Umgebung ihre
Software und deren Abhängigkeiten läuft. Diese Container-Umgebung ist von anderen Anwendungen isoliert.
Weiterhin lässt sich die Installation der Anwendung damit leichter automatisieren und Upgrades sind einfacher
durchzuführen. Es existieren Werkzeuge, die das Installieren von containerisierten Anwendungen vereinfachen.
Eins davon ist die die Co-op Cloud mit dem Automatisierungswerkzeug Abra.

- **SSO**: Single-Sign-On, ermöglicht eine gute Integration von mehreren verschiedenen Tools. Denn mit
  SSO muss sich ein:e User:in nur einmal mit den Zugangsdaten einloggen und hat dann Zugriff auf alle
  angebundenen Tools. Dieses Feature verbessert die
  User Experience wesentlich. Im ersten Teil dieses
  Berichts wurde der Bedarf nach einem Multitool und
  Vereinheitlichung deutlich. Mit SSO kann ein Schritt
  in diese Richtung gegangen werden. SSO kann mit
  unterschiedlichen Standards realisiert werden. Dabei
  sind openid-connect und ldap die verbreitetsten.
- **Entwicklung**: Am besten steht hinter einem Softwareprojekt eine ganze Community, die regelmäßig
  Verbesserungen an der Software veröffentlicht. Häufig hängt ein Projekt aber auch nur von einer einzel-
  nen Person oder einem kommerziellen Unternehmen
  ab. Ein Unternehmen kann sowohl die Entwicklung
  stark fördern, als aber auch die Gefahr einer Kommerzialisierung der Software bergen. In den meisten
  von uns untersuchten Tools gab es glücklicherweise
  rege Aktivität in der Entwicklung. Deswegen haben
  wir nur bei Projekten, die nicht mehr aktiv entwickelt
  werden, dies extra erwähnt. Bei der Auswahl eines
  Tools ist dieser Punkt entscheidend, da nur bei einer
  aktiven Entwicklung sichergestellt ist, dass dieses
  Tool auch über einen längeren Zeitraum mit den unablässigen Fehlerkorrekturen und ggf. auch mit funk-
  tionellen Verbesserungen versorgt wird.

## Contribute

Wir freuen uns über deinen Beitrag, öffne ein Issue, Pullrequest oder kontaktiere uns via [Matrix](https://matrix.to/#/!WplKrvvevUnClTDkwm:matrix.local-it.org?via=matrix.local-it.org) oder [E-Mail](mailto:info@local-it.org)

## Referenzen

- [Awesome-Selfhosted List](https://github.com/awesome-selfhosted/awesome-selfhosted)
- [Coop-Cloud List](https://apps.coopcloud.tech/)
- [Yunohost](https://yunohost.org/de/apps?q=%2Fapps)
- [Cloudron](https://www.cloudron.io/store/index.html)
- [solidarische landwirtschaft](https://www.solidarische-landwirtschaft.org/solawis-aufbauen/it-infrastruktur/software#accordionHead139)
- https://directory.fsf.org/wiki/Main_Page

[^gnu]: https://www.gnu.org/philosophy/free-sw.de.html#n1
[^osd]: https://opensource.org/docs/osd
[^mit]: https://choosealicense.com/licenses/mit/
[^choosealicense]: https://choosealicense.com/
