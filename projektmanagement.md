# Project Management Tools

## Kanban

Eine beliebte und vielseitige Methode zum Projektmanagement ist Kanban. Es wurde ursprünglich in Japan von Toyota entwickelt und wird mittlerweile in vielen Variationen angewendet.
Es gibt eine große Anzahl von Tools, die Kanban Boards
unterstützen. Viele bieten noch deutlich mehr Funktionalitäten und sind teilweise auf bestimmte Projektmanagementmethoden wie z.B. Scrum ausgerichtet. Die Folgende Liste an Tools ist eine kleine Auswahl. Es muss nicht
unbedingt nachteilig sein, wenn ein Tool nicht alle Funktionen hat, wenn ein reines Kanbanboard gewünscht ist
und z.B. zur Zeiterfassung ein anderes Tool verwendet
wird.

|               | Redmine            | Wekan           | Kanboard         | Vikunja           | Taiga                  |
| ------------- | ------------------ | --------------- | ---------------- | ----------------- | ---------------------- |
| Lizenz        | 🕊️                 | 🕊️              | 🕊️               | 🕊️                | 🕊️                     |
|               | GPL-V2             | MIT             | MIT              | AGPL-V3           | AGPL-V3                |
| Installation  | S                  | S               | S                | S                 | S                      |
| SSO           | ✅ plugin          | ✅              | ✅ plugin        | ✅                | ✅ plugin              |
| Entwicklung   | Jean-Philippe Lang | Lauri Osjansivu | Frédéric Guillot | Konrad Langenberg | Kaleidos Ventures S.L. |
| Kanban        | plugin             | ✅              | ✅               | ✅                | ✅                     |
| Tickets       | ✅                 | ❌              | ✅               | ✅                | ✅                     |
| Gantt         | ✅                 | ❌              | ✅ plugin        | ✅                | ❌                     |
| Kalender      | ✅                 | ✅              | ✅               | ✅                | ❌                     |
| Wiki          | ✅                 | ❌              | ✅ plugin        | ❌                | ✅                     |
| Zeiterfassung | ✅                 | ❌              | ✅               | ❌                | ❌                     |

### Redmine

Redmine[^redmine] ist ein vielseitiges Projektmanagement-Werkzeug mit Ticketsystem, Wiki, Zeiterfassung und allen erdenklichen Funktionen. Das Design ist schon etwas älter,
kann aber über diverse Plugins angepasst werden. Allerdings benötigt das Tool eine erhebliche Einarbeitung und
zu Beginn muss bei einer neuen Installation sehr viel konfiguriert werden.

### Wekan

Wekan[^wekan] ist eine Alternative zu dem bekannten kommerziellen Tool Trello.
Boards können universell eingesetzt
werden und es wird wenig Struktur vorgegeben. Organisationen,
die bereits mit Trello Erfahrungen gesammelt
haben, finden in Wekan eine ähnliche Basisfunktionalität
und eine ähnlich gestaltete Bedienoberfläche. Es gibt aber
anders als bei Trello nur wenige Erweiterungen. Unsere
Erfahrung zeigt, dass die Ladezeiten der Boards bei vielen
Karten mit Anhängen teilweise lange sein können. Wekan
ist weniger für klassisches Projektmanagement geeignet.

### Kanboard

Das Kanboard[^kanboard] bildet den klassischen Kanbanprozess
ab. Es hat unterschiedliche Analysefunktionen und ist
recht leichtgewichtig. Das Standard-Design ist nicht mehr
so modern, kann aber angepasst werden. Anzumerken
ist, dass sich die Software im „Maintenance Mode“ befindet, damit zwar noch Sicherheitsupdates erhält, aber
keine neuen Features mehr entwickelt werden. Dies liegt
vermutlich daran, dass die Software im wesentlichen von
einem einzelnen Entwickler entwickelt wurde und wird,
der nicht mehr ausreichend Ressourcen einbringen kann
oder will. Dies ist ein ganz typisches Problem von Open-Source-Projekten
und sollte bei der Auswahl immer mit
betrachtet werden. Über die Pluginschnitstelle kann Kanboard
aber durch viele Plugins im Funktionsumfang erweitert werden.

### Vikunja

Vikunja[^vikunja] ist als simple persönliche ToDo Liste gestartet,
bietet mittlerweile aber auch umfangreiche Funktionen
wie Kanban oder Gantt Ansichten. Es hat ein modernes
Design und lässt sich intuitiv bedienen. Ein gleichzeitige
Bearbeitung ist nicht sinnvoll möglich, da es keine automatische
Aktualisierung geänderter Inhalte gibt.

### Taiga

Taiga[^taiga] ist ein Projektmanagement Tool, das stark auf die
Scrum Methode für agile Softwareprojekte ausgerichtet
ist. Das Backlog, Wiki, Epics, Userstories, Issues, aber
natürlich auch ein Kanban Board machen Taiga vollständig.

### Nextcloud Deck

Die Nextcloud bietet mit der App Deck[^nextcloud] ebenfalls ein
simples Kanban-Board. Wenn Nextcloud bereits vorhanden ist,
kann Deck ein schneller Ersatz für Notizzettel
sein. Für eine weitergehende Nutzung bietet Deck aber
nicht genug Funktionen.

[^redmine]: https://redmine.org/
[^wekan]: https://wekan.github.io/
[^kanboard]: https://kanboard.org
[^vikunja]: https://vikunja.io/
[^taiga]: https://www.taiga.io/
[^nextcloud]: https://apps.nextcloud.com/apps/deck
