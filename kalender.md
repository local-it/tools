# Kalender

Ein gemeinsamer Kalender ist für viele Teams ein zentrales Element in der Arbeitsorganisation. Viele E-Mail- oder
Groupware-Lösungen integrieren bereits Kalender. Neben
den bereits im Abschnitt „E-Mail“ betrachteten Lösungen
wird noch das Tool Bloben vorgestellt. Es werden folgende Funktionen betrachtet:

1. CalDav: Ein Netzwerkprotokoll zur Synchronisierung
   von Kalendern
2. Einbindung Externer Kalender: Externe Kalender können z.B. über Webcal / iCal eingebunden werden.
3. Mehrere Kalender pro User
4. Verfügbarkeitsplaner: Beim Erstellen eines Termins kann die Verfügbarkeit von den eingeladenen
   User:innen eingesehen werden.
5. Teilen von Kalendern: Kalender können mit anderen
   User:innen geteilt werden und z.B. nur lesender Zugriff gewährt werden.
6. Email-Einladungen versenden: Einladungen können
   per Email versendet werden.

| Funktion                     | SOGo | Roundcube | EGroupware       | Nextcloud | bloben |
| ---------------------------- | ---- | --------- | ---------------- | --------- | ------ |
| CalDav                       | ✔️   | ✔️        | ✔️               | ✔️        | ✔️     |
| Einbindung Externer Kalender | ✔️   | ❌        | ✔️               | ✔️        | ✔️     |
| Mehrere Kalender pro User    | ✔️   | ✔️        | nur über Gruppen | ✔️        | ✔️     |
| Verfügbarkeitsplaner         | ✔️   | ❌        | ✔️               | ✔️        | ❌     |
| Teilen                       | ✔️   | ✔️        | ✔️               | ✔️        | ✔️     |
| Email Einladungen            | ✔️   | ✔️        | ✔️               | ✔️        | ✔️     |

## Bloben

Bloben[^bloben] ist im Gegensatz zu den anderen Lösungen ein
reiner Kalenderclient und benötigt eine Verbindung zu
einem CalDav-Server. Dafür ist es darauf ausgerichtet,
mehrere Kalender darzustellen und überzeugt mit einem
modernen Design. Bloben ist zwar noch im Beta-Entwicklungsstadium, scheint aber die Nachfolgerin für AgenDAV,
das nur noch im Maintenance-Modus ist, zu werden.

## Nextcloud Kalender App

Die Nextcloud Kalender-App[^nextcloud] integriert sich sehr gut mit
anderen Apps in der Nextcloud und bringt alle Funktionen,
die sich User:innen wünschen. Die Darstellung ist einfach
und erfüllt ihre Funktion. Auch wenn andere Tools umfangreichere Darstellungen und Hervorhebungen haben,
ist der Nextcloud-Kalender dennoch für die tägliche Arbeit
ausreichend.

## EGroupware

Der Kalender ist in EGroupware[^egroupware] eine der Kernfunktionen, geht allerdings von dem Ansatz aus, dass ein:e
Benutzer:in einen eigenen Kalender hat und sonst Gruppenkalender verwendet. Kalender können untereinander
freigegeben werden. Die Gruppenfunktion ist der EPL vorbehalten, kann aber von den Admins über die Datenbank
auch in der CE benutzt werden. Es ist allerdings darauf
hinzuweisen, dass dies nur ein Workaround ist.

## SOGo

Der Kalender von SOGo[^sogo] ist übersichtlich, lässt sich intuitiv bedienen und sieht schick aus. Er unterstützt alle
benötigten Funktionen und integriert sich gut mit der EMail- und Kontakte-Funktion.

## Roundcube

Roundcubes[^roundcube] Kalender macht einen guten Eindruck, ist
einfach zu bedienen und hat die wichtigsten Funktionen.

[^bloben]: https://bloben.com/
[^nextcloud]: https://apps.nextcloud.com/apps/calendar
[^egroupware]: https://www.egroupware.org/
[^sogo]: https://www.sogo.nu/
[^roundcube]: https://roundcube.net/
