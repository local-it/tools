# Dateiablage

Eine grundlegende Aktivität für das kollaborative Arbeiten im Team, ist der Austausch von Dateien (Dokumente,
Bilder, Videos, Audio, etc.). Einfache Methoden sind der
Versand per E-Mail, Messenger oder gar per USB-Stick.
Schnell steigen aber die Anforderungen an einer Dateiversionierung, wenn z.B. mehrere an einem Dokument arbeiten und eine Datei überarbeitet hin und her geschickt wird
oder ein alter Stand einer Datei wiederhergestellt werden
soll. Auch ist es oft gewünscht, Dateien öffentlich verfügbar machen zu können sowie viele weitere Funktionen.
In der Softwareentwicklung ist das Programm Git[^git] mit
Plattformen wie Github, Gitlab, Gitea, Bitbucket usw. für
die Versionierung üblich. Es gibt zwar auch grafische
Oberflächen für Git, es bedarf aber durch die spezielle
Funktionsweise eine längere Einarbeitung und ist daher
abseits der Softwareentwicklung selten für Teams geeignet.
Für Dokumentenaustausch mittels Peer-to-Peer-Kommunikation (p2p), also Verbindungen zwischen Clients die
ohne zentralen Server auskommen, kann sich in kleinen
Gruppen das Programm Syncthing[^syncthing] anbieten. Konfiguriert
wird es über eine lokale Weboberfläche, die aber ein eher
technisches Publikum anspricht und eine gewisse Eingewöhnung braucht.
In klassischen Firmennetzwerken wird häufig Samba[^samba]
eingesetzt. Es basiert auf demselben Protokoll wie Dateinetzwerke von Microsoft Windows. Mit Samba können
Dateien und Ordner von einem zentralen Server oder
Rechnern im selben Netzwerk freigegeben und geteilt
werden. Der Zugriff geschieht dabei abhängig vom Betriebssystem mit dem jeweiligen Dateiexplorer. Möglich
ist auch ein Zugriff aus anderen Netzwerken mittels eines
Virtual-Private-Network (VPN) und damit der Zugriff von
verschiedenen Standorten aus.
Cloudspeicher-Lösungen werden auf einem Server betrieben und können überall aus dem Internet erreicht werden.
Herkömmliche Anbieter sind beispielsweise Dropbox,
Google Drive oder Microsoft Onedrive. Als Open-Source-Alternativen wurden die Tools Nextcloud, Seafile und EGroupware getestet.

## Überblick Cloudspeicher

|              | Nextcloud      | Seafile      | EGroupware      |
| ------------ | -------------- | ------------ | --------------- |
| Lizenz       | 🕊️             | 🕊️ / 💲      | 🕊️ / 💲         |
|              | AGPL           | GPL / EPL    | GPL / EPL       |
| Installation | S              | S            | M / L           |
| SSO          | ✔️             | ✔️           | ❌              |
| Entwicklung  | Nextcloud GmbH | Seafile Ltd. | Egroupware GmbH |

## Funktioneller Vergleich

1. Integration einer kollaborativen Textverarbeitung
   (Markdown – Textbasierte Auszeichnung von Überschriften, Aufzählungen etc., Collabora Office - Online-Version von Libre-Office oder OnlyOffice – angelehnt an Microsoft Office), als womöglich wichtigste
   Kombination für ein Multi-Tool.
2. Integration in das Betriebssystem (Es gibt Synchronisations-Anwendungen für den PC und mobile Apps),
   die den Zugriff auf gemeinsame Dateien ohne oder
   mit sehr einfachen lokalen Tools erlaubt. Außerdem
   ist damit eine lokale Speicherung möglich, die den
   offline Zugriff ermöglicht.
3. Dateien können per WebDAV synchronisiert werden.
   WebDAV ist ein standardisiertes Protokoll für den webbasierten Austausch von Dateien und ermöglicht
   eine sehr flexible und transparente Einbindung in das
   Betriebssystem und weitere Anwendungen.
4. Funktionelle Erweiterbarkeit durch Plugins o.ä. und
   damit die Erweiterbarkeit zu einem Multi-Tool.

| Funktionen                        | Google Drive | Nextcloud  | Seafile    | EGroupware |
| --------------------------------- | ------------ | ---------- | ---------- | ---------- |
| 1. Kollaborative Textverarbeitung | ✔️           | MD, CA, OO | MD, CA, OO | CA         |
| 2. Syncclient                     | ✔️           | ✔️         | ✔️         | ❌         |
| 3. Webdav                         | ❌           | ✔️         | ✔️         | ✔️         |
| 4. Erweiterbarkeit                | ❌           | ✔️         | ❌         | ❌         |

**MD**: Markdown **CA**: Collabora **OO**: OnlyOffice

### Nextcloud

Die Nextcloud[^nextcloud] kann mit ihrer großen Bekanntheit und auch großen Community punkten. Sie bietet viele
Funktionalitäten und ist durch eine vielzahl von Apps erweiterbar. Damit kann Nextcloud für einige Gruppen
auch schon als Alleinlösung ausreichend sein. Allerdings ist die Qualität und der Funktionsumfang der Apps stark unterschiedlich, weshalb es lohnen kann spezialisierte Tools stattdessen einzusetzen. Collabora, Onlyoffice und ein Markdown Editor könnten zur kollaborativen Dokumentenbearbeitung in Nextcloud integriert werden.
In der reinen Dateisynchronisierung schneidet Nextcloud in der Performance etwas schlechter ab als andere.

### Seafile

Im Gegensatz zu Nextcloud verfolgt Seafile[^seafile] den Ansatz:
“do one thing and do it well”. Seafile hat deutlich weniger
Funktionen als Nextcloud, aber dafür eine performantere
Dateisynchronisation. Die Dokumentenbearbeitung mit
Collabora, Onlyoffice und Markdown ist ebenfalls möglich. Bei der Installation gab es kleinere Schwierigkeiten
in der Konfiguration, die sich aber durch Automatisierung
verhindern lassen.

Seafile wird von einem kommerziellen Unternehmen mit
Sitz in Bejing (China) entwickelt und finanziert sich über
eine EPL. Fraglich ist, wie sehr bei der Entwicklung Rücksicht auf die Community genommen wird. Die „Community Edition“ steht auf Github zur Verfügung. Allerdings sind
dort nur sehr wenige Entwicklungsaktivitäten erkennbar.
Wer bereits Nextcloud kennt, hat gegebenenfalls zu Beginn etwas Probleme mit der anderen Bedienung und
Terminologie von Seafile. Auch könnte besser ersichtlich
sein, welche Ordner und Dateien ein:e User:in freigegeben
hat.

### EGroupware

EGroupware[^egroupware] hat den Schwerpunkt, wie der Name schon
vermuten lässt, stärker auf Groupware, wie E-Mail, Kalender, Kontakte und weiteres. Neben einer Office-Integration
mit Collabora und Jitsi Video, gibt es aber auch eine Dateiablage. Allerdings ist es in der freien Version von EGroupware nicht möglich Ordner- oder Dateien zu teilen und
diese kann deswegen nicht mit Seafile oder Nextcloud
mithalten. Außerdem ist EGroupware weniger modular
und externe Entwickler:innen können nicht so leicht über
Apps Funktionen hinzufügen. Das Unternehmen ist eine
deutsche GmbH, die sich durch EPL-Lizenzen finanziert.
Das Design ist etwas in die Tage gekommen und kann für
neue User:innen durch die große Zahl von Funktionen unübersichtlich und abschreckend wirken. Da EGroupware
als alleinstehende Suite konzeptioniert wurde, ist es nicht
möglich eine externe Authentifizierung anzubinden. Sie
kann aber als SSO-Provider eingerichtet werden und andere Apps anbinden. In einem Test verlief die Installation
recht hakelig. Dabei gab es Probleme mit der Collabora Integration, die zur Fehlfunktion der gesamten Suite führte.

### Weiteres

Eine weitere Lösung ist Pydio[^pydio], sie wurde noch nicht näher betrachtet, macht aber einen sehr modernen Eindruck, bietet aber viele relevante Features auch nur in EPL an.

Die Plattform Peergos[^peergos] sieht ebenfalls viel versprechend aus, ist aber noch in Entwicklung und nicht für den produktiven Gebrauch gedacht.


[^git]:https://git-scm.com/
[^syncthing]: https://syncthing.net/
[^samba]: https://www.samba.org/
[^nextcloud]: https://nextcloud.com/
[^seafile]: https://www.seafile.com
[^egroupware]: https://www.egroupware.org/
[^pydio]: https://pydio.com/
[^peergos]: https://peergos.org/