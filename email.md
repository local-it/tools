# E-Mail

Wie auch die Bedarfsanalyse gezeigt hat, ist E-Mail als
Kommunikationsmittel immer noch unersetzlich. Häufig
wird E-Mail aber auch als Lösung für Probleme verwendet,
die ein spezialisiertes Tool besser lösen könnte (Dateiablage,
kollaborative Dokumentenbearbeitung, Ticketsystem,
Social-Media, …). Deshalb sollte bedacht werden,
wofür der Einsatz von E-Mail sinnvoll ist und ob die
Aufgabe nicht durch den Einsatz eines anderen Tools besser
gelöst werden kann.
Klassischerweise verbindet man sich mittels E-Mail-Client
mit einem E-Mail-Server. Einige der gängigsten Clients
sind Microsoft Outlook[^outlook] oder als Open-Source-Alternative
Thunderbird[^thunderbird] und Evolution[^evolution]. Da auch das Einrichten
eines E-Mail Kontos für einige Anwender:innen schon
eine Hürde darstellt, werden Webmail-Clients wie z.B. bei
Google-Mail, die einfach über den Webbrowser aufgerufen
werden können, immer beliebter.
Ein großer Vorteil von E-Mail ist die dezentrale Architektur
des Email-Protokolls, die den Betrieb eigener E-Mail
Server ermöglicht. Allerdings ist der Betrieb für Serverbetreiber:innen
nicht trivial, denn durch die lange Historie
gibt es viele Tücken zu beachten. Im laufenden Betrieb
ist es wichtig, gegen Spam-Nachrichten vorzugehen.
Gleichzeitig muss auch die sogenannte „Reputation“ des
eigenen E-Mail Servers im Blick behalten werden. Gerade
Betreiber:innen von eigenen Email-Servern haben es
schwer, von den großen etablierten Betreiber:innen (wie
Google) akzeptiert zu werden und nicht auf deren
Spamliste zu landen oder durch „Greylisting“ und andere
Spam- und Phishing-Vermeidungsmethoden (SPF, DKIM) unter
großen Verzögerungen bei der Zustellung zu leiden. Das
macht die Administration sehr aufwändig und es bedarf
der regelmäßigen Überprüfung der Reputation und
Kommunikation mit den entsprechenden E-Mail Anbietern.
Abhilfe können All-in-one-Lösungen schaffen, die die
Best-Practices beim Betrieb eines E-Mail Servers zu
automatisieren versuchen und Hilfestellung bei der korrekten
Konfiguration geben.
Im folgenden werden zunächst vier
All-in-one-Mail-Server aus Betreiber:innen-Perspektive verglichen. Danach
kommt ein Vergleich von Webmail-Clients aus User:innen Perspektive.

### Server

#### Überblick

|                 | Mailu     | Mailcow   | modoboa     | IRedMail       |
| --------------- | --------- | --------- | ----------- | -------------- |
| Lizenz          | 🕊️        | 🕊️        | 🕊️          | 🕊️ / 💲        |
|                 | MIT       | GPL-3.0   | ISC License | GPL-3.0        |
| Installation    | S         | M         | L           | L              |
| SSO             | ❌        | ❌        | LDAP        | LDAP           |
| Webmail Clients | RL RC     | SG        | modoboa     | SG RC          |
| Entwicklung     | Community | Servercow | NGYNLABS    | IRedMail d.o.o |

**SG**: SOGo,
**RL**: Rainloop,
**RC**: Roundcube,

#### Mailu

Mailu[^mailu] hat ein übersichtliches Admin-Interface und bietet alle
wichtigen Funktionen für die Administration eines
Mailservers. Als Webclient kann entweder Rainloop oder
Roundcube verwendet werden. Für eine externe Authentifizierung
ist ein Fork mit einer „openid connect“-Implementierung in Entwicklung.

#### Mailcow

Mailcow[^mailcow] lässt sich über Docker-Compose problemlos
installieren und updaten. Die Administrationsoberfläche
bietet sehr viele Funktionen und praktische Helferlein für
die Administration des E-Mail Servers. Als Webmail-Client
wird SOGo verwendet. Mailcow kann selbst als SSO Provider (OAuth)
dienen und andere Apps anbinden. Die An-
bindung von Mailcow an einen SSO-Provider ist aber
nicht möglich und auch nicht geplant.

#### Modoboa

Ein Setup mit Docker steht für Modoboa[^modoboa] noch nicht zur
Verfügung, es wird mit einem Python-basierten Installer
eingerichtet, der alle benötigten Komponenten auf dem
Server direkt installiert. Modoboa kann in der Installation
mit einem externen Nutzer:innen-Verzeichnis (LDAP) kon-
figuriert werden und es ist somit möglich, es an andere
Authentifizierungssysteme anzubinden. Es kommt mit
einem eigenen Webmailclient mit Apps für Kontakte und
Kalender, aber sehr geringem Funktionsumfang. Die
Administrationsoberfläche bietet Administrator:innen eine
gute Kontrolle über Postfächer und Domains und hat wie
Mailcow auch gute Helfer z.B. zur Konfiguration von DNS-
Einträgen.

#### IRedMail

Das Docker-Setup von IRedMail[^iredmail] ist als „experimental“
gekennzeichnet und noch nicht für den Produktiveinsatz
gedacht. Die Administrationsoberfläche ist sehr simpel
gehalten und hat nur grundlegende Funktionen. IRedMail
kann wie Modoboa zusammen mit einem OpenLDAP ins-
talliert werden. Dieser kann manuell so konfiguriert wer-
den, dass er auch für eine externe Authentifizierung (SSO)
verwendet werden kann.

#### Integration von E-Mail Server mit anderen Apps auf einem Server

Im Gegensatz zu den meisten anderen Tools ist das Setup
eines E-Mail Servers sehr viel aufwendiger. Da mehrere
Ports benötigt werden, gehen Autoinstaller davon aus,
dass ein Mailserver auf einem eigenen Server ohne weitere
Anwendungen installiert wird. Die Integration auf einen
Server mit anderen Anwendungen in Kombination mit
einem weiteren Reverse-Proxy ist grundsätzlich möglich,
muss aber im Einzelfall geprüft werden. Für eine Integration
mit Single-Sign-On ist LDAP der verbreitetste Weg, es
gibt aber auch neuere Entwicklungen, die auf XOAUTH2
basieren und damit „openid-connect“ ermöglichen. Dies
muss aber noch manuell konfiguriert werden und nicht
alle Email-Server / Clients unterstützen diese Funktion.

### Clients

|              | SOGo    | Rainloop  | Roundcube   | EGroupware      | Nextcloud      |
| ------------ | ------- | --------- | ----------- | --------------- | -------------- |
| Lizenz       | 🕊️      | 🕊️        | 🕊️ / 💲     | 🕊️ / 💲         | 🕊️             |
|              | GPL-2.0 | AGPL-3.0  | GPL-3.0     | GPL-2.0         | AGPL-3.0       |
| Installation | S       | S         | S           | ML              | S              |
| SSO          | ✔️      | ❌        | ✔️          | ❌              | ✔️             |
| Entwicklung  | Alinto  | Community | Apheleia IT | Egroupware GmbH | Nextcloud GmbH |

Um auf seine E-Mails zugreifen zu können, wird ein E-Mail
Client benötigt. Neben den gängigen Funktionen wie E-Mail-Versand
und -Empfang haben wir in unserem Vergleich
folgende Funktionen betrachtet:

1. **E-Mail**
   - Mehrere E-Mail Accounts: Es ist möglich mit einem
     Login mehrere E-Mail Accounts einzubinden.
   - Terminiertes versenden: Eine E-Mail kann zu einem
     späteren Zeitpunkt versendet werden.
   - Integration von Anhängen: Anhänge können aus ei-
     nem anderen Tool ausgewählt oder abgelegt werden.
2. **Adressbuch**
3. **E2E-Verschlüsselung**: Es ist möglich Ende-zu-Ende
   Verschlüsselte E-Mails zu lesen und versenden.

|                             | SOGo | Rainloop | Roundcube | EGroupware | Nextcloud         |
| --------------------------- | ---- | -------- | --------- | ---------- | ----------------- |
| 1. E-mail                   | ✔️   | ✔️       | ✔️        | ✔️         | ✔️                |
| .. Mehrere E-Mail Accounts  | ✔️   | ✔️       | ❌        | ✔️         | ✔️                |
| .. Terminiertes Versenden   | ❌   | ❌       | 💲        | ❌         | ✔️                |
| .. Integration von Anhängen | ❌   | Dropbox  | 💲        | ✔️         | ✔️                |
| 2. Adressbuch               | ✔️   | ✔️       | ✔️        | ✔️         | ✔️                |
| 3. E2E-Verschlüsselung      | ❌   | ✔️       | ✔️        | ✔️         | Mailvelope Plugin |

#### SOGo

SOGo[^sogo] macht mit seinem modernen Design einen guten
Eindruck. Es lässt sich intuitiv bedienen und bietet die
wichtigsten Funktionen. Es fehlt aber die Integration für
E2E-Verschlüsselung und Anhänge von einem Cloudspeicher (WebDAV).

#### Rainloop

Rainloop[^rainloop] bietet nur Grundfunktionalitäten und ist recht
simpel, aber dafür übersichtlich gehalten. Es fehlt ein
Kalender und das Design ist etwas veraltet, aber intuitiv.
Rainloop kann auch in Nextcloud integriert werden, allerdings
ist die Integration noch nicht ganz ausgereift. Als
leichtgewichtige und auf Geschwindigkeit optimierte
Weiterentwicklung kann der Fork snappymail.eu von Interesse sein.

#### Roundcube

Roundcube[^roundcube] macht einen guten Eindruck und bietet einen großen Funktionsumfang durch Plugins, die aber leider größtenteils nicht frei verfügbar sind.

#### EGroupware

EGroupware[^egroupware] sticht durch die Menge an möglichen Integrationen
und Funktionen heraus. Die Bedienung ist aber
sehr gewöhnungsbedürftig und das User-Interface wirkt
beim Einstieg sehr überfordernd. Als Gesamtlösung kann
für Benutzer:innen mehr vorkonfiguriert werden, so kön-
nen z.B. E-Mail-Konten global konfiguriert werden und diese
dann einem oder mehreren Benutzer:innen zugewiesen
werden. Weiter kann die Benutzeroberfläche global vorgestaltet
werden und je nach Bedarf den Benutzer:innen
mehr oder weniger Freiheiten gegeben werden.

#### Nextcloud

Die Email-[^ncm] und Adresssbuch-Apps[^ncc] für Nextcloud können mit
der praktischen Integration in den Cloudspeicher
punkten und bieten viele Features. Die Bedienung ist aber
nicht sehr intuitiv, hakelig und teils fehleranfällig. Auch
muss das E-Mail Konto manuell verknüpft werden. In der
Praxis hat sich bisher die E-Mail-App in der Nextcloud als
nette Zusatzfunktion aber nicht für die tägliche Arbeit bewährt.
Menschen, die viel mit E-Mails arbeiten, sind komfortablere
Oberflächen und umfangreichere Funktionen
gewöhnt.

[^outlook]: https://outlook.live.com/owa/
[^thunderbird]: https://www.thunderbird.net/de/
[^evolution]: https://help.gnome.org/users/evolution/stable/
[^mailu]: https://mailu.io/
[^mailcow]: https://mailcow.email/
[^modoboa]: https://modoboa.org
[^iredmail]: https://www.iredmail.org/
[^sogo]: https://www.sogo.nu/
[^rainloop]: https://www.rainloop.net/
[^roundcube]: https://roundcube.net/
[^egroupware]: https://www.egroupware.org/
[^ncm]: https://apps.nextcloud.com/apps/mail
[^ncc]: https://apps.nextcloud.com/apps/contacts
