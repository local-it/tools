# Videokonferenzsysteme

Wenn Gruppen von verschiedenen Orten der Welt sich virtuell treffen möchten,
um Gespräche in Gruppen zu führen,
um Schulungen zu ermöglichen oder Interviews durchzuführen,
bieten sich Videokonferenzsysteme an. Neben Video-
und Audioübertragung gibt es oft den Bedarf etwas
präsentieren zu wollen, was z.B. als Screensharing, einem
Whiteboard, einer eingebetteten Präsentation oder in einem
gestreamten Video passiert. Insbesondere für Konferenzen,
Schulungen oder interaktive Angebote haben
sich Videokonferenzsysteme bewährt, die auf Räumen
basieren. Dabei werden virtuelle Räume dauerhaft oder
bei Bedarf erstellt, in die Teilnehmer:innen über einen Link
und manchmal mit optionalen Zugangscodes eingeladen
werden. Teilnehmer:innen können asynchron den Raum
betreten und können sich ab diesem Zeitpunkt untereinander
sehen und hören. Funktionale Überschneidungen
gibt es mit Messengern, die Videotelefonie unterstützen.
Die weltweit meist genutzte, kommerzielle Videokonferenzlösung
ist Zoom. Diese nutzen wir hier als Referenz.
Wir betrachten drei freie Software-Alternativen: BigBlue-
Button, Jitsi Meet und Apache OpenMeetings.
Eine weitere Möglichkeit ist die Nextcloud-App Talk[^nextcloud].
Diese ist allerdings mehr ein Messenger mit Videofunktion,
als eine Videokonferenzlösung, weshalb sie nicht im Vergleich
aufgeführt wird. Außerdem ist Nextcloud Talk zur
Zeit nur für eine kleine Teilnehmer:innenzahl zu empfehlen.

|                     | BigBlueButton      | Jitsi Meet       | Apache OpenMeetings |
| ------------------- | ------------------ | ---------------- | ------------------- |
| Lizenz              | 🕊️                 | 🕊️               | 🕊️                  |
|                     | LGPL-3.0           | Apache 2.0       | Apache 2.0          |
| Installation        | L                  | L                | XL                  |
| SSO                 | ✅                 | ✅               | ✅                  |
| Entwicklung         | BigBlueButton inc. | 8x8 Inc.         | Apache              |
| Öffentlicher Zugang | ✅                 | ✅               | ❌                  |
| Moderne Oberfläche  | ✅                 | ✅               | ❌                  |
| Chat                | ✅                 | ✅               | ✅                  |
| Pad                 | ✅                 | ✅               | ❌                  |
| Whiteboard          | ✅                 | ❌               | ✅                  |
| Screenshare         | ✅                 | ✅               | ✅                  |
| Telefon Dial-In     | ✅                 | ✅               | ✅                  |
| Breakouträume       | ✅                 | ✅               | ❌                  |
| Recording           | ✅                 | ✅               | ✅                  |
| Kalender            | ❌                 | Google/Microsoft | ✅                  |
| Abstimmungen        | ✅                 | ❌               | ✅                  |

### BigBlueButton

BigBlueButton (BBB)[^bbb] ist eine webbasierte Videokonferenzlösung
mit dem Fokus auf Bildungseinrichtungen,
Schulungen und Lehrveranstaltungen. In den virtuellen
Räumen von BBB gibt es verschiedene Ansichten die den
Fokus entweder auf eine Präsentation, ein kollaboratives
Whiteboard, einen geteilten Bildschirm oder die Videos
der Teilnehmenden setzt. Neben Lehrveranstaltungen
eignet sich BBB aufgrund seiner ausgereiften Plattform
und der flexiblen Funktionen ebenfalls zur Nachbildung
eines virtuellen Konferenzraumes. Rauminterne Chats,
Umfragefunktionen und einige andere Extras runden das
Bild ab. BBB unterstützt auch das Aufzeichnen von Konferenzen
teilweise, wenn dies durch die Administration freigeschaltet ist.

### Jitsi Meet

Bei Jitsi Meet[^jitsi] werden im Gegensatz zu BBB die virtuellen
Räume in der Regel nicht vorkonfiguriert. Der virtuelle
Raum entsteht erst mit Beginn einer Konferenz. Dabei
wird der Raumname selbst gewählt und ist Teil der URL.
Die erste Person im Raum kann Berechtigungen setzen,
die so lange gelten, wie jemand in diesem Raum ist. Im
Standard ist ein Jitsi Meet Server offen und jede:r kann
ihn verwenden. Um eine eigene Instanz auf eine Organisation
einzuschränken, kann eine Authentifizierung aktiviert
werden. Dann können User:innen ohne Login erst teilnehmen,
sobald ein Mitglied den Raum betreten hat.

### Apache OpenMeetings

Apache OpenMeetings[^apache] ist nicht so bekannt und verbreitet
wie Jitsi und BBB und könnte doch eine interessante Alternative
sein oder werden. Der Fokus von Apache’s
OpenMeetings liegt deutlich auf der Nachbildung eines
realen Konferenzraumes. Die Administration kann Konferenzäume
vorbereiten, die für einen bestimmten Zweck
optimiert sind. So kann ein Whiteboard für die gemeinsame
Arbeit, ähnlich wie bei BBB, im Zentrum sein oder
z.B. ein Interview-Modus, bei dem Bewerbergespräche
geführt und (nach deren Zustimmung) aufgezeichnet
werden können. Optisch wirkt die Oberfläche veraltet, auf
den zweiten Blick allerdings recht flexibel. Das Design ist
nachrangig in der Entwicklung. Insgesamt macht OpenMeetings
eher den Eindruck einer Plattform, die erst noch durch
weitere Programmier- und Designarbeit für eine Organisation
angepasst werden muss.

[^nextcloud]: https://nextcloud.com/talk/
[^bbb]: https://bigbluebutton.org/
[^jitsi]: https://jitsi.org/
[^apache]: https://openmeetings.apache.org/