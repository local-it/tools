# Dokumentenbearbeitung

Bei der Bearbeitung von Dokumenten lässt sich zwischen
Offline-Bearbeitung und kollaborativem Schreiben unterscheiden. Auch die verwendeten Dateiformate unterscheiden sich. Prinzipiell unterscheiden sich die Tools auch in der Darstellung von Text. Auf der einen Seite stehen Tools, die einfach nur auf der Tastatur eingegebene
Zeichen ohne Formatierung darstellen und auf der anderen Seite die sogenannten WYSIWYG-Tools - „What You See Is What You Get“ – die umfangreiche Textformatierung erlauben, die direkt am Bildschirm sichtbar sind.
Letzteres sind die bekannten Tools wie Microsoft Word
oder LibreOffice (Open-Source). Erstere Tools erlauben
teilweise aber die Übersetzung von bestimmten Textelementen in Text-Formatierung, so dass auch damit Überschriften, Aufzählungen, Fett-Schreibung etc. möglich
sind.

Als „Plaintext“ bezeichnet werden Dokumente bezeichnet,
die Text ganz ohne Formatierung oder anderen applikationsspezifischen Inhalt speichern. Für etwas mehr Struktur sorgen sog. vereinfachte Auszeichnungssprachen wie
zum Beispiel ReStructuredText[^rst] oder Markdown[^markdownguide]. Diese verwenden Textelemente, die das Dokument in reiner
Form schon leicht lesbar und bearbeitbar machen, aber in
verschiedene andere Formate (HTML, PDF, …) exportiert
werden können. Auch die gleichzeitge Darstellung der formatierten Darstellung am Bildschirm steht meist zur Verfügung. Die verwendete Sprache ist recht leicht zu lernen
und bietet die wichtigsten Funktionalitäten für Texte. Mit
Erweiterungen wie z.B. PlantUML ist es möglich auch Diagramme usw. zu generieren, das benötigt aber eine erweiterte und damit komplexere Sprache. Markdown hat eine
sehr weite Verbreitung in Wikis, Blogs, Programmierplattformen wie Github oder Stackoverflow. Es gibt spezielle
Tools, die den Einstieg und die Nutzung von Markdown
erheblich erleichtern, da sie Unterstützung für die richtige
Anwendung der Sprache bieten und formatierte Darstellung und Export in andere Formate direkt zur Verfügung
stellen wie z.B. Zettlr.[^zettlr]
Microsoft Office und die freie Software-Alternative Openoffice[^openoffice] bzw. Libreoffice[^libreoffice] sind Programme die nach dem
What-you-see-is-what-you-get (WYSIWYG)-Prinzip arbeiten. Das heißt während der:die Autor:in ein Dokument
bearbeitet, sieht er:sie bereits, wie es fertig formatiert
aussieht. Aufgrund der einfachen Bedienbarkeit und großem Funktionsumfang wie Tabellenkalkulation, Präsentationen etc. haben diese Programm-Pakete eine große
Verbreitung erlangt. Im Gegensatz zu Markdown ist man
allerdings stark an das Dateiformart der Applikation gebunden und kann die Dokumente nur in der Anwendung
selbst betrachten. Vor allem das Microsoft-Office-Dateiformat ist proprietär und damit teils problematisch in andere zu konvertieren.

In der wissenschaftlichen Arbeit ist das Softwarepaket
LaTex[^latex], das auf dem Textsatzsystem Tex aufbaut, weitverbreitet. Hier arbeitet der:die Autor:in mit einer Art Programmiersprache und kann nach der Berechnung der
Ausgabe das fertig formatierte Dokument verwenden und
ist damit im Grunde vergleichbar mit MarkDown aber erheblich komplexer. Latex eignet sich für hohe Ansprüche
an die Formatierung, wie z.B. Mathematische Formeln,
komplexere Textsätze, Dissertationen und insbesondere
in Kombination mit Literaturprogrammen. Latex hat eine
hohe Einarbeitungszeit, bietet dafür aber mehr Stabilität
und mehr Berechenbarkeit beim Ergebnis und Erleichterung bei der Generierung von Literatur-/ Abbildungsverzeichnissen.
Auch für das kollaborative Arbeiten ist es oft vorteilhafter offline an Dokumenten zu arbeiten. Online ist man abhängig von der Internetverbindung und hat gerade bei den
WYSIWYG-Tools eine größere Latenz, d.h. die Tools fühlen
sich „zäh“ an. In Kombination mit einem Cloudspeicher
können offline bearbeitete Dateien wieder synchronisiert
werden. Bearbeiten allerdings mehrere gleichzeitig die
gleiche Datei, kann es zu Widersprüchen in der Bearbeitung kommen, die dann manuell oder mit Unterstützung
der Tools wieder behoben werden müssen. Deswegen
lohnt es sich zu prüfen, ob ein Online-Tool notwendig ist
und wenn ja, ob dieses vom Typ „WYSIWYG“ sein muss.
Im Folgenden werden fünf verschiedene Lösungen mit
unterschiedlichen Anwendungsbereichen vorgestellt.

## Online Kollaborativ

|              | Etherpad                | Hedgedoc              | Cryptpad  | Onlyoffice      | Collabora              |
| ------------ | ----------------------- | --------------------- | --------- | --------------- | ---------------------- |
| Lizenz       | 🕊️                      | 🕊️                    | 🕊️        | 🕊️ / 💲         | 🕊️ / 💲                |
|              | Apache 2.0              | AGPLv3                | AGPL v3   | MIT             | MPL 2.0                |
| Installation | S                       | S                     | S         | S               | S                      |
| SSO          | ✅                      | ✅                    | ❌        | -               | -                      |
| Entwicklung  | The Etherpad Foundation | David Mehren und Team | XWiki SAS | Ascensio System | Collabora Productivity |

## Markdown Editoren und Pads

Wie bereits erwähnt, bieten sich für simple Textverarbeitung leichtgewichtigere Tools an. So genannte Pads ermöglichen das kollaborative Arbeiten von vielen Autor:innen am selben Dokument mit simpler Formatierung.
Gerade wenn zunächst der Inhalt und nicht die visuelle
Darstellung im Vordergrund stehen, haben diese Tools
viele Vorteile. In der Regel haben diese Tools keine spürbare Latenz.

### Etherpad

Etherpad[^etherpad] ist das älteste und am weitesten verbreitete
Tool zum kollaborativen und im Wortsinne gleichzeitigen
Schreiben. Es ist sehr einfach aufgebaut und fokussiert
auf das Wesentliche: Text schreiben. Weiterhin ist sehr
leicht nachvollziehbar wer was geschrieben hat, da jede:r
Autor:in eine eigene Textfarbe erhält. Die Revisions-, Import-, Export-Funktionen, ein Chat und die Möglichkeit,
Pads öffentlich in einem reinen Lesemodus zu teilen, machen das Tool vielseitig verwendbar. Durch Plugins lässt
sich Etherpad auch noch im Funktionsumfang erweitern.
Möchte man ein Dokument allerdings aufwendiger formatieren, kommt man mit Etherpad schnell an Grenzen und
exportiert den Text nach der Kollaborationsphase besser
in ein anderes Format.

### Hedgedoc

Hedgedoc[^hedgedoc] ist quasi wie ein Etherpad nur mit deutlich
mehr Funktionen und nutzt Markdown um Textformatierungen zu ermöglichen. Es bietet eine laufend aktualisierte Ansicht des formatierten Textes, die das Erlernen erleichtert und die Überprüfung der korrekten Formatierung
des Textes erlaubt. Weiterhin gibt es viele Erweiterungen,
um z.B. Diagramme oder mathematische Formeln darzustellen oder das Dokument als Präsentation zu öffnen.
Das integrierte Usermanagement zeigt alle Pads / Dokumente eine:r User:in an und kann auch per SSO angebunden werden.

### Cryptpad

Wie der Name schon andeutet, legt Cryptpad[^cryptpad] großen
Wert auf die Verschlüsselung der Dokumente. Neben
Markdown Pads (Code genannt) bietet Cryptpad weitere
Tools zur Dokumentenbearbeitung wie einen Texteditor
(WYSIWYG) und Tabellenkalkulation über eine clientseitige
Onlyoffice-Integration, Formulare, Kanban Boards und
Whiteboards. Es gibt ebenfalls ein Account-Management.
Eine externe Authentifizierung per SSO ist zur Zeit noch
nicht möglich befindet sich aber in der Planung.

## Office-Suites

Für die kollaborative Bearbeitung von Dokumenten nach
dem WYSIWYG-Prinzip gibt es zwei verbreitete Lösungen:
Onlyoffice und Collabora. Beide haben verschiedene
Lizenzmodelle, die die Nutzung für eine kleinere Anzahl
von User:innen kostenfrei in einer Community Edition
ermöglichen und bei einer größeren Zahl von
Anwender:innen kostenpflichtig sind. Leider kam es in unseren Tests
sowohl bei Collabora, als auch bei Onlyoffice zu fehlerhaftem
Verhalten, vor allem sobald mehrere Nutzer:innen
gleichzeitig ein Dokument bearbeiteten.

### Collabora

Collabora[^collabora] ist eine Lösung, die serverseitig die Office-Suite
LibreOffice ausführt und dieses dann über ein Webinterface
anbietet. Da die Inhalte anders als bei Onlyoffice vom
Server gestreamt werden, kann es zu größeren Latenzen
kommen. Die Open-Source-Version „Collabora Online Development
Edition“ (CODE) wird von den Herstellern nicht
für den Produktiveinsatz und auch nur für kleine Gruppen
bzw. „Home Use“ empfohlen. Das liegt auch daran, dass
es keine als stabil klassifizierten Veröffentlichungen gibt.
Die Entwickler:innen von Nextcloud fokussieren bei der Integration
von Office-Tools allerdings stärker auf Collabora
als auf Onlyoffice. Positiv anzumerken ist, dass Collabora
viel zum LibreOffice-Projekt beiträgt. Unsere Erfahrungen
zeigen, dass Collabora insbesondere wegen der Latenz
weniger flüssig als Onlyoffice läuft.

### Onlyoffice

Der Onlyoffice[^onlyoffice] Document-Server (Docs) kann z.B. in
Nextcloud integriert werden oder als Teil des Onlyoffice-Workspace
verwendet werden. Dabei handelt es sich um
eine umfangreichere eigene Software-Suite. Onlyoffice
orientiert sich stärker an dem Design von Microsoft-Office
und nutzt auch dessen Dateiformat. Es verbraucht etwas
weniger Server-Ressourcen als Collabora, da es überwiegend
auf den Clients läuft. Die kostenlose Version bietet
bis zu 20 simultane Verbindungen/Nutzer:innen. Eine Lizenz
für z.B. 50 Nutzer:innen kostet 1200€ einmalig. In
der Standardeinstellung gibt es eine potenzielle Fehlerquelle:
Onlyoffice speichert Änderungen zuerst in einer
eigenen Datenbank und synchronisiert sie erst zurück zu
Nextcloud wenn kein:e Nutzer:in das Dokument mehr geöffnet
hat. Da dies zu Irritationen und Datenverlust führen
kann, ist es möglich, eine Option zu aktivieren, die Änderungen
direkt in der Nextcloud speichert („Force Save“).

[^rst]: https://docutils.sourceforge.io/rst.html
[^markdownguide]: https://www.markdownguide.org/
[^zettlr]: https://www.zettlr.com/
[^openoffice]: https://www.openoffice.org/
[^libreoffice]: https://de.libreoffice.org/
[^latex]: https://www.latex-project.org/
[^etherpad]: https://etherpad.org/
[^hedgedoc]: https://hedgedoc.org/
[^cryptpad]: https://cryptpad.org/
[^onlyoffice]: https://www.onlyoffice.com/
[^collabora]: https://www.collabora.com/
